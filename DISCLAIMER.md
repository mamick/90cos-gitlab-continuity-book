# Disclaimer
  
# External Links Disclaimer

The appearance of external links on this site does not constitute official endorsement on behalf of the 90th Cyberspace Operations Squadron, the U.S. Department of the Air Force, or the Department of Defense.
The Department of the Air Force does not endorse any non-federal entities, products, or services.
The Department of the Air Force does not exercise any responsibility or oversight of the content at external link destinations.

# Overview
90COS public GitLab is provided on behalf of the 90th Cyberspace Operations Squadron.
Any links are provided for educational and informative purposes only.

# Privacy Act Statement
If you choose to provide us with personal information - such as filling out a GitLab issue with an e-mail address or providing your GitLab username - we only use that information to respond to your message or request. We will only share the information you give us with another government agency if your inquiry relates to that agency, or as otherwise required by law. We never create individual profiles or give it to any private organizations. We never collect information for commercial marketing. Though you must provide an e-mail address or GitLab username for a response, we recommend that you NOT include any other personal information. 90COS is a group within GitLab. More information on GitLab Privacy policies may be found at https://about.gitlab.com/privacy/ 

If you have any questions or comments about the information presented here, please e-mail us at 90IOS.DOT.INBOX@us.af.mil
 

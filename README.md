# 90COS Gitlab Continuity Book  

This project contains the rules, policies, and guidance for the administration of the 90COS gitlab project.

## Rules

### Group and Project Ownership Policy

Please familiarize yourself with Gitlab permission documentation here: https://docs.gitlab.com/ee/user/permissions.html

**Only Government personnel may be granted "Owner" rights.** Because this repository holds our offical work products it is impertitive that we maintain positive control.  Government personnel shall not grant Owner rights to contractor personnel.  It is highly recommended that **Maintainer** permissions also remain within the government, but contractors may hold maintainer rights in limited cercumstances where warranted.







See this brief... needs to be put into something related to this book (https://docs.google.com/presentation/d/1CgSq6-YBkJhS_x3T7UGpmbclGh0evmIyMkb1ZWi3ViQ/edit?usp=sharing)





TODO: Need to consider how to be multi-flight... almost exclusively engineered to CYT right now.

